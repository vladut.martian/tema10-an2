<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index(Request $request)
    {

        $product = new Product();
        $form = $this->createFormBuilder($product)
        ->add('name', TextType::class)
        ->add('Amount', IntegerType::class)
        ->add('Price', IntegerType::class)
        ->add('Category', TextType::class)
        ->add('Utility', TextType::class)
        ->add('save', SubmitType::class, ['label' => 'Submit'])
        ->getForm();

        $form->handleRequest($request);
        $succes = 0 ;

        if($form->isSubmitted() && $form->isValid()){
            $customer = $form->getData();
            $succes = 1;
            $enitityManager = $this->getDoctrine()->getManager();
            $enitityManager->persist($product);
            $enitityManager->flush();

        }  else {
            $succes = 2;
        }

        return $this->render('product/index.html.twig', [
            'form' => $form->createView(),
           'succes' => $succes,
        ]);
    }
}
